package br.ucsal.bes20201.testequalidade.locadora;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	public static final String DEFAULT_PLACA = "LUX0S66";
	public static final Integer DEFAULT_ANO = 2019;
	public static final Modelo DEFAULT_MODELO = new Modelo("AUDI A3");
	public static final Double DEFAULT_VALOR_DIARIA = 135.00;
	public static final SituacaoVeiculoEnum DEFAULT_SITUACAO_VEICULO = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = DEFAULT_PLACA;

	private Integer ano = DEFAULT_ANO;

	private Modelo modelo = DEFAULT_MODELO;

	private Double valorDiaria = DEFAULT_VALOR_DIARIA;

	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO_VEICULO;
	
    public VeiculoBuilder() {}

	public static VeiculoBuilder criar() {
		return new VeiculoBuilder();
	}
	
	public static VeiculoBuilder veiculoDefault() {
        return VeiculoBuilder.criar()
            .comPlaca(DEFAULT_PLACA)
            .comAno(DEFAULT_ANO)
            .comModelo(DEFAULT_MODELO)
            .comValorDiaria(DEFAULT_VALOR_DIARIA)
            .comSituacao();
    }

	public static VeiculoBuilder veiculoDisponivel() {
        return VeiculoBuilder.veiculoDefault()
            .comSituDisponivel();
    }
	public static VeiculoBuilder veiculoManutencao() {
        return VeiculoBuilder.veiculoDefault()
            .comSituManutencao();
    }
	public static VeiculoBuilder veiculoLocado() {
        return VeiculoBuilder.veiculoDefault()
            .comSituLocado();
    }

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comSituDisponivel() {
        this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
        return this;
    }
	public VeiculoBuilder comSituManutencao() {
        this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
        return this;
    }
	public VeiculoBuilder comSituLocado() {
        this.situacao = SituacaoVeiculoEnum.LOCADO;
        return this;
    }
	
	public VeiculoBuilder comSituacao() {
		return this;
	} 
 
    public Veiculo build() {
        Veiculo veiculo = new Veiculo();
        veiculo.setAno(this.ano);
        veiculo.setModelo(this.modelo);
        veiculo.setPlaca(this.placa);
        veiculo.setSituacao(this.situacao);
        veiculo.setValorDiaria(this.valorDiaria);
         
		return veiculo;
    }
}
