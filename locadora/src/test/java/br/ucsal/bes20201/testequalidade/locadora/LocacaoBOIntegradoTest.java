package br.ucsal.bes20201.testequalidade.locadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano
	 * de fabricaçao e 3 veículos com 8 anos de fabricação.
	 */

	

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {

		VeiculoDAO.insert(new Veiculo(VeiculoBuilder.veiculoDefault().build()));
		VeiculoDAO.insert(new Veiculo(new VeiculoBuilder().comAno(2019).comModelo(new Modelo("BMW S�rie 5")).comPlaca("LUX0S64")
				.comSituManutencao().comValorDiaria(332.00).build()));
		VeiculoDAO.insert(new Veiculo(new VeiculoBuilder().comAno(2012).comModelo(new Modelo("Jagar XJ")).comPlaca("LUX0S79")
				.comSituLocado().comValorDiaria(179.00).build()));
		VeiculoDAO.insert(new Veiculo(new VeiculoBuilder().comAno(2012).comModelo(new Modelo("Rolls-Royce")).comPlaca("LUX0S99")
				.comSituDisponivel().comValorDiaria(1500.00).build()));
		VeiculoDAO.insert(new Veiculo(new VeiculoBuilder().comAno(2012).comModelo(new Modelo("Tesla Roadster")).comPlaca("LUX0S69").comSituLocado()
				.comValorDiaria(112.00).build()));
		
		Double resultadoEsp = 6236.70;
		Assertions.assertEquals(resultadoEsp, LocacaoBO.calcularValorTotalLocacao(VeiculoDAO.getVeiculos(), 3));
	}

}
























